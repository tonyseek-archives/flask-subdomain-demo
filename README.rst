Flask Subdomain Demo
====================

|Build Status|

Those server names should be configured in ``/etc/hosts`` and nginx:

- ``www.orz.dev``
- ``accounts.orz.dev``


.. |Build Status| image:: https://drone.io/bitbucket.org/tonyseek/flask-subdomain-demo/status.png
   :target: https://drone.io/bitbucket.org/tonyseek/flask-subdomain-demo/latest
   :alt: Build Status
