from flask import Blueprint, url_for


bp = Blueprint('home', __name__)


@bp.route('/')
def index():
    return url_for('accounts.login')
