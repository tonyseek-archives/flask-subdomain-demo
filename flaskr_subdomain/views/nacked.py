from flask import Blueprint, url_for, redirect


bp = Blueprint('nacked', __name__, subdomain='')


@bp.route('/')
def index():
    return redirect(url_for('home.index'))
