from flask import Blueprint, url_for


bp = Blueprint('accounts', __name__, subdomain='accounts')


@bp.route('/login')
def login():
    return url_for('home.index')
