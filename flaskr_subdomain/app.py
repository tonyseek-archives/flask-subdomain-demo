from flask import Flask

from .views import accounts, home, nacked


def create_app(config=None):
    app = Flask(__name__)
    app.config.from_object('envcfg.json.demo')
    app.config.from_object(config)
    app.url_map.default_subdomain = app.config.get('DEFAULT_SUBDOMAIN', '')
    app.register_blueprint(accounts.bp)
    app.register_blueprint(home.bp)
    app.register_blueprint(nacked.bp)
    return app
