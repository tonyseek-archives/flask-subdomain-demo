def test_home(flask_client):
    r = flask_client.get('http://www.example.com/')
    assert r.status_code == 200
    assert r.data == 'http://accounts.example.com/login'


def test_account(flask_client):
    r = flask_client.get('http://accounts.example.com/login')
    assert r.status_code == 200
    assert r.data == 'http://www.example.com/'


def test_not_found(flask_client):
    r = flask_client.get('http://accounts.example.com/')
    assert r.status_code == 404

    r = flask_client.get('http://unknown.example.com/')
    assert r.status_code == 404


def test_nacked(flask_client):
    r = flask_client.get('http://example.com/')
    assert r.status_code == 302
    assert r.headers['Location'] == 'http://www.example.com/'
