from pytest import yield_fixture, fixture

from flaskr_subdomain.app import create_app


class TestingEnvironment(object):
    TESTING = True
    SERVER_NAME = 'example.com'
    DEFAULT_SUBDOMAIN = 'www'


@yield_fixture
def flask_app():
    app = create_app(TestingEnvironment())
    with app.app_context():
        yield app


@fixture
def flask_client(flask_app):
    return flask_app.test_client()
